import { controls } from '../../constants/controls';

// keys that is down pressed at current moment
var currentDownKeys = new Set();
var currentFirstFighterHealth;
var currentSecondFighterHealth;

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    //copy obj properties , so that original dont change
    firstFighter = { ...firstFighter };
    secondFighter = { ...secondFighter };
    //init fighters health
    let isplayerOneCombinationTimerLeft = true;
    let isplayerTwoCombinationTimerLeft = true;
    currentFirstFighterHealth = firstFighter.health;
    currentSecondFighterHealth = secondFighter.health;

    const removeEventListenersFromDocument = () => {
      document.removeEventListener('keydown', keyDownHandler, false);
      document.removeEventListener('keyup', keyUpHandler, false);
    };
    const getRoundProcent = (numder) => (Math.round(numder * 100) > 0 ? Math.round(numder * 100) : 0);
    const healthChecker = () => {
      if (currentFirstFighterHealth <= 0) {
        removeEventListenersFromDocument();
        resolve(secondFighter);
      }
      if (currentSecondFighterHealth <= 0) {
        removeEventListenersFromDocument();
        resolve(firstFighter);
      }
      renderHealthBarLeft(getRoundProcent(currentFirstFighterHealth / firstFighter.health));
      renderHealthBarRight(getRoundProcent(currentSecondFighterHealth / secondFighter.health));
    };
    const keyDownHandler = (event) => {
      currentDownKeys.add(event.code);
      let isPlayerOneCriticalHitCombinationComplete = true;
      let isPlayerTwoCriticalHitCombinationComplete = true;
      controls.PlayerOneCriticalHitCombination.forEach((el) => {
        currentDownKeys.has(el) ? null : (isPlayerOneCriticalHitCombinationComplete = false);
      });
      controls.PlayerTwoCriticalHitCombination.forEach((el) => {
        currentDownKeys.has(el) ? null : (isPlayerTwoCriticalHitCombinationComplete = false);
      });

      if (
        !currentDownKeys.has(controls.PlayerOneBlock) &&
        isPlayerOneCriticalHitCombinationComplete &&
        isplayerOneCombinationTimerLeft
      ) {
        currentSecondFighterHealth -= firstFighter.attack * 2;
        isplayerOneCombinationTimerLeft = false;
        setTimeout(() => (isplayerOneCombinationTimerLeft = true), 10 * 1000);
      }
      if (
        !currentDownKeys.has(controls.PlayerTwoBlock) &&
        isPlayerTwoCriticalHitCombinationComplete &&
        isplayerTwoCombinationTimerLeft
      ) {
        currentFirstFighterHealth -= secondFighter.attack * 2;
        isplayerTwoCombinationTimerLeft = false;
        setTimeout(() => (isplayerTwoCombinationTimerLeft = true), 10 * 1000);
      }
      healthChecker();
    };
    const keyUpHandler = (event) => {
      currentDownKeys.delete(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack:
          currentSecondFighterHealth -=
            !currentDownKeys.has(controls.PlayerTwoBlock) && !currentDownKeys.has(controls.PlayerOneBlock)
              ? getDamage(firstFighter, secondFighter)
              : 0;
          break;
        case controls.PlayerTwoAttack:
          currentFirstFighterHealth -=
            !currentDownKeys.has(controls.PlayerTwoBlock) && !currentDownKeys.has(controls.PlayerOneBlock)
              ? getDamage(secondFighter, firstFighter)
              : 0;
          break;
      }
      healthChecker();
    };

    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('keyup', keyUpHandler, false);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}

function renderHealthBarLeft(procent) {
  return renderHealthBar('left', procent);
}
function renderHealthBarRight(procent) {
  return renderHealthBar('right', procent);
}
function renderHealthBar(position, procent) {
  let healthBar = document.querySelector(`#${position}-fighter-indicator`);

  healthBar.style.width = `${procent}%`;
}
