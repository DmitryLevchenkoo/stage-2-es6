import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  /*
   * When we select the first fighter, the second is undefined
   */
  if (fighter) {
    const fighterImage = createFighterImage(fighter);

    const fighterDetails = createFighterDetails(fighter);
    fighterElement.append(fighterImage, fighterDetails);
  }
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
function createFighterDetails(fighter) {
  const fighterDetailInfoWrapper = createElement({
    tagName: 'div',
    className: `fighter-preview-detail__items-wrapper`,
  });
  const fighterItems = Object.entries(fighter)
    .filter(([key]) => key != '_id' && key != 'source')
    .map(([key, value]) => createFighterItem(key, value));

  fighterDetailInfoWrapper.append(...fighterItems);

  return fighterDetailInfoWrapper;
}
function createFighterItem(key, value) {
  let atributeElement = createElement({ tagName: 'p', className: 'fighter-preview-detail__atribute' });
  let valueElement = createElement({ tagName: 'p', className: 'fighter-preview-detail__value' });
  let itemElement = createElement({ tagName: 'div', className: 'fighter-preview-detail__item' });
  atributeElement.innerHTML = key;
  valueElement.innerHTML = value;

  itemElement.append(atributeElement, valueElement);

  return itemElement;
}
