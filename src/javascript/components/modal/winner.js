import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';
export function showWinnerModal(fighter) {
  // call showModal function
  showModal({ title: `${fighter.name} Win!!!`, bodyElement: getBodyElement(fighter), onClose: onModalClose });
}
function onModalClose() {
  let arenaElement = document.querySelector('#root');
  arenaElement.innerHTML = '';

  return new App();
}

function getBodyElement(fighter) {
  const bodyContentElement = createElement({ tagName: 'div', className: 'modal-body_content' });
  const textElement = createElement({ tagName: 'p', className: 'modal-body_text' });
  const text = `Fighter ${fighter.name} wins, congrats`;
  textElement.innerHTML = text.toUpperCase();
  bodyContentElement.appendChild(textElement);

  return bodyContentElement;
}
